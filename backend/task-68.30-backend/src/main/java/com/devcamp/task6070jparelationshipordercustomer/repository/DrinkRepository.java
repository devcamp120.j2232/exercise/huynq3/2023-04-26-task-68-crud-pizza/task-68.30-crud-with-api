package com.devcamp.task6070jparelationshipordercustomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6070jparelationshipordercustomer.model.Drink;

public interface DrinkRepository extends JpaRepository<Drink, Long>  {
    
}
