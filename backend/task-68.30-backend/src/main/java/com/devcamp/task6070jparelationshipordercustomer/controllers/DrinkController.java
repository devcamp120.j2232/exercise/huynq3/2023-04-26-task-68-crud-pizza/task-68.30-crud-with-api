package com.devcamp.task6070jparelationshipordercustomer.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task6070jparelationshipordercustomer.model.Drink;
import com.devcamp.task6070jparelationshipordercustomer.repository.DrinkRepository;
import com.devcamp.task6070jparelationshipordercustomer.service.DrinkService;


@RestController
@RequestMapping("/drink")
@CrossOrigin
public class DrinkController {
    @Autowired
    DrinkService drinkService;
    
    @GetMapping("/all")
    public ResponseEntity<List<Drink>> getAllDrinks(){
        try {
            return new ResponseEntity<>(drinkService.getAllDrinks(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("detail/{id}")
    public ResponseEntity<Drink> getDrinkById(@PathVariable("id") long id){
        try {
            Drink drink = drinkService.getDrinkById(id);
            if (drink != null){
                return new ResponseEntity<>(drink, HttpStatus.OK);
            }
            else  return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @Autowired
    DrinkRepository drinkRepository;
    @PostMapping("/create")
	public ResponseEntity<Drink> createDrink(@RequestBody Drink pDrink) {
		try {
            pDrink.setNgayTao(new Date());
            pDrink.setNgayCapNhat(null);
			Drink _drink = drinkRepository.save(pDrink);

			return new ResponseEntity<>(_drink, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @PutMapping("/update/{id}")
	public ResponseEntity<Drink> updateDrinkById(@PathVariable("id") long id, @RequestBody Drink pDrink) {
        try {
            Optional<Drink> drinkData = drinkRepository.findById(id);
            if (drinkData.isPresent()) {
                Drink drink = drinkData.get();
                drink.setMaNuocUong(pDrink.getMaNuocUong());
                drink.setTenNuocUong(pDrink.getTenNuocUong());
                drink.setPrice(pDrink.getPrice());
                drink.setNgayCapNhat(new Date());

                return new ResponseEntity<>(drinkRepository.save(drink), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
	}

    @DeleteMapping("/delete/{id}")
	public ResponseEntity<Drink> deleteDrinkById(@PathVariable("id") long id) {
		try {
			drinkRepository.deleteById(id);

			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


}

